import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import Menu from './component/Menu';
import MyCard from './component/MyCard';

export default function App(){
  return (
    <div>
      <Menu />
      <MyCard />
    </div>
  )

}