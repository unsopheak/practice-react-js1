import React, { Component } from 'react'
import {Card,Button,} from 'react-bootstrap'

export default class MyCard extends Component {
    constructor(){
      super()
      this.state=[
      {
        image:"https://upload.wikimedia.org/wikipedia/commons/thumb/4/44/Ankor_Wat_temple.jpg/1200px-Ankor_Wat_temple.jpg",
        title:"Angkor",
        des:"In cambodia"
      },
      {
        image:"https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Ta_Prohm_%28III%29.jpg/1200px-Ta_Prohm_%28III%29.jpg",
        title:"Taprom temple",
        des:"In cambodia"
      },
      {
        image:"https://upload.wikimedia.org/wikipedia/commons/3/3c/Banteay_Srei_full2.jpg",
        title:"Banteay srei temple",
        des:"In cambodia"
       },
]
    }
   render() {
     let myCard = this.state.map((item) =>{
       console.log("ITEM:",item) 
       return(
                  <Card style={{ width: '18rem',float:'left'}}>
                  <Card.Img variant="top" src={item.image}/>
                  <Card.Body>
                    <Card.Title>{item.title}</Card.Title>
                    <Card.Text>
                      {item.des}
                    </Card.Text>
                    <Button variant="danger">Delete</Button>
                  </Card.Body>
                </Card>
       );               
     });
       return (
          <div>
            {myCard}
           </div>
        )
   }
}
